import getUrl from "./getUrl";
import useCookies from "./cookies";
import { ref } from "@vue/reactivity";
import { useRouter } from "vue-router";
import axios from "axios";
import { ElMessage } from "element-plus";
import useErorrs from "./errors";

const useAuth = () => {
  const { urlRegister, urlLogin, urlLogout } = getUrl();
  const { setCookie, clearSession, setCookieWithEncode, getDecodedCookie } =
    useCookies();
  const { wrongEmailOrPassword } = useErorrs();
  const router = useRouter();
  const loading = ref(false);

  const storeUser = (payload) => {
    loading.value = true;

    axios
      .post(urlRegister, payload)
      .then((res) => {
        ElMessage({
          type: "success",
          message: res.data.message,
        });
        router.push({ name: "Login" });
      })
      .catch((err) => {
        ElMessage({
          type: "error",
          message: err.response.data.message,
        });
      })
      .then(() => {
        loading.value = false;
      });
  };

  const loginUser = async (payload) => {
    loading.value = true;

    try {
      const res = await axios.post(urlLogin, payload);
      const { access_token, id, expired, id_role } = await res.data.data;
      await setCookieWithEncode("session", res.data.data, expired);

      router.push({ name: "Home" });
    } catch (err) {
      wrongEmailOrPassword(err.response.data.message);
    } finally {
      loading.value = false;
    }
  };

  const logoutUser = () => {
    const session = getDecodedCookie("session");
    const token = session.access_token;

    axios.defaults.headers.common = {
      Authorization: `Bearer ${token}`,
      "x-user-status": "office",
    };
    axios.post(urlLogout).then((res) => {
      clearSession();
      router.replace({ name: "Login" });
    });
  };

  return {
    loading,
    storeUser,
    loginUser,
    logoutUser,
  };
};

export default useAuth;
