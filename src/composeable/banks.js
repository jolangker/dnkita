import axios from "axios";
import { ElMessage } from "element-plus";
import { reactive, ref } from "vue";
import { useRouter } from "vue-router";
import useCookies from "./cookies";
import useErorrs from "./errors";
import getUrl from "./getUrl";

const useBanks = () => {
  const { urlBanks } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError, forbidden } = useErorrs();

  const router = useRouter();
  const banks = ref([]);
  const bank = ref({});
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;
  const idRole = session.id_role;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getBanks = async () => {
    loading.value = true;
    try {
      const res = await axios.get(urlBanks);
      banks.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const getBank = async (id) => {
    loading.value = true;
    try {
      const res = await axios.get(`${urlBanks}/${id}`);
      bank.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const storeBank = async (payload) => {
    loading.value = true;

    try {
      const res = await axios.post(urlBanks, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Bank" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const updateBank = async (id, payload) => {
    loading.value = true;

    try {
      const res = await axios.post(`${urlBanks}/${id}`, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Bank Details", params: { id } });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const destroyBank = async (id) => {
    loading.value = true;
    try {
      const res = await axios.delete(`${urlBanks}/${id}`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      router.push({ name: "Master Bank" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  return {
    banks,
    bank,
    loading,
    getBanks,
    getBank,
    storeBank,
    updateBank,
    destroyBank,
  };
};

export default useBanks;
