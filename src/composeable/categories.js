import { ref, reactive } from "vue-demi";
import axios from "axios";
import getUrl from "./getUrl";
import useCookies from "./cookies";
import useErorrs from "./errors";
import { useRouter } from "vue-router";
import { ElMessage } from "element-plus";

const useCategories = () => {
  const { urlCategories } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError, forbidden } = useErorrs();
  const categories = ref([]);
  const category = ref({});
  const loading = ref(false);
  const router = useRouter();

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getCategories = async () => {
    loading.value = true;
    try {
      const res = await axios.get(urlCategories);
      categories.value = res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err.response.data.message);
    } finally {
      loading.value = false;
    }
  };

  const getCategory = async (id) => {
    loading.value = true;
    try {
      const res = await axios.get(`${urlCategories}/${id}`);
      category.value = res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err.response.data.message);
    } finally {
      loading.value = false;
    }
  };

  const storeCategories = async (payload, image) => {
    loading.value = true;
    try {
      const res = await axios.post(`${urlCategories}`, payload);
      await storeCategoriesImage(res.data.data.id, image);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Kategori" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const storeCategoriesImage = async (id, payload) => {
    const res = await axios.post(`${urlCategories}/${id}`, payload);
  };

  const updateCategory = async (id, payload, image) => {
    loading.value = true;

    try {
      const res = await axios.put(`${urlCategories}/${id}`, payload);
      if (image.get("gambar")) {
        await storeCategoriesImage(id, image);
      }
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Kategori Details", params: { id } });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const destroyCategory = async (id) => {
    loading.value = true;
    try {
      const res = await axios.delete(`${urlCategories}/${id}`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      router.push({ name: "Master Kategori" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  return {
    categories,
    category,
    loading,
    getCategory,
    getCategories,
    storeCategories,
    updateCategory,
    destroyCategory,
  };
};

export default useCategories;
