import axios from "axios";
import { ElMessage } from "element-plus";
import { ref, reactive } from "vue";
import { useRouter } from "vue-router";
import useCookies from "./cookies";
import useErrors from "./errors";
import getUrl from "./getUrl";

const useCities = () => {
  const { urlCities } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError, forbidden } = useErrors();
  const router = useRouter();
  const cities = ref([]);
  const city = ref({});
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getCities = async () => {
    loading.value = true;
    try {
      const res = await axios.get(urlCities);
      cities.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const getCity = async (id) => {
    loading.value = true;
    try {
      const res = await axios.get(`${urlCities}/${id}`);
      city.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const storeCity = async (payload) => {
    loading.value = true;

    try {
      const res = await axios.post(urlCities, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Kabupaten Kota" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const updateCity = async (id, payload) => {
    loading.value = true;

    try {
      const res = await axios.put(`${urlCities}/${id}`, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({
        name: "Master Kabupaten Kota Details",
        params: { id },
      });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const destroyCity = async (id) => {
    loading.value = true;

    try {
      const res = await axios.delete(`${urlCities}/${id}`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Kabupaten Kota" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  return {
    cities,
    city,
    loading,
    getCities,
    getCity,
    storeCity,
    updateCity,
    destroyCity,
  };
};

export default useCities;
