const useCookies = () => {
  const setCookie = (name, value, expired) => {
    document.cookie = `${name}=${value}; expires=${new Date(expired)};path=/`;
  };

  const getCookie = (name) => {
    return (
      document.cookie.match("(^|;)\\s*" + name + "\\s*=\\s*([^;]+)")?.pop() ||
      ""
    );
  };

  const clearSession = () => {
    document.cookie = "session=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT";
  };

  const setCookieWithEncode = (name, source, expired) => {
    const te = new TextEncoder();
    const str = JSON.stringify(source);
    const encoded = te.encode(str);
    setCookie(name, encoded, expired);
  };

  const getDecodedCookie = (name) => {
    const td = new TextDecoder();
    const str = getCookie(name);

    if (str) {
      const arr = str.split(",");
      const encoded = new Uint8Array(arr);

      try {
        return JSON.parse(td.decode(encoded));
      } catch (err) {
        clearSession();
      }
    } else {
      return false;
    }
  };

  return {
    setCookie,
    getCookie,
    clearSession,
    setCookieWithEncode,
    getDecodedCookie,
  };
};

export default useCookies;
