import { reactive, ref } from "vue-demi";
import getUrl from "./getUrl";
import useCookies from "./cookies";
import axios from "axios";
import useErorrs from "./errors";
import moment from "moment";

const useDashboard = () => {
  const { urlDashboard } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized } = useErorrs();

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  const datasets = ref({});
  const loading = ref(false);

  const getDashboard = (startDate, endDate) => {
    loading.value = true;
    axios.defaults.headers.common = headers;

    axios
      .get(urlDashboard, {
        params: {
          start_date: startDate,
          end_date: endDate,
        },
      })
      .then((res) => {
        datasets.value = res.data.data;
      })
      .catch((err) => {
        if (err.response.status === 401) {
          unauthorized();
        }
      })
      .then(() => {
        loading.value = false;
      });
  };

  return {
    datasets,
    loading,
    getDashboard,
  };
};

export default useDashboard;
