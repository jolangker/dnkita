import useCookies from "./cookies";
import { ElMessage } from "element-plus";
import { useRouter } from "vue-router";

const useErorrs = () => {
  const { clearSession } = useCookies();
  const router = useRouter();

  const unauthorized = () => {
    ElMessage({
      type: "error",
      message: "Sesi berakhir. Harap login kembali",
    });
    clearSession();
    router.push({ name: "Login" });
  };

  const forbidden = () => {
    ElMessage({
      type: "error",
      message: "Maaf, anda tidak dapat melakukan aksi tersebut.",
    });
  };

  const wrongEmailOrPassword = (message) => {
    ElMessage({
      type: "error",
      message,
    });
  };

  const othersError = (message) => {
    ElMessage({
      type: "error",
      message,
    });
  };

  const invalidFileFormat = () => {
    ElMessage({
      type: "warning",
      message: "Format file harus png, jgp ,jpeg",
    });
  };

  const exceededUpload = () => {
    ElMessage({
      type: "warning",
      message: "Gambar tidak boleh lebih dari 1",
    });
  };

  const entityError = (message) => {
    ElMessage({
      type: "error",
      message,
    });
  };

  return {
    unauthorized,
    forbidden,
    wrongEmailOrPassword,
    othersError,
    invalidFileFormat,
    exceededUpload,
    entityError,
  };
};

export default useErorrs;
