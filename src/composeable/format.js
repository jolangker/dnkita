import moment from "moment";

const useConvert = () => {
  const toRupiah = (value) =>
    Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    }).format(value);

  const toRupiahTable = (row, column, cellValue) =>
    Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    }).format(cellValue);

  const toDateTimeTable = (row, column, cellValue) =>
    moment(row.created_at).format("DD-MM-YYYY HH:mm:ss");

  const toDateTable = (row, column, cellValue) =>
    moment(cellValue).format("DD-MM-YYYY");

  const formatDate = (value) => moment(value).format("DD-MM-YYYY");

  const formatTag = (status) => {
    if (status == 51 || status == 91) return "success";
    if (
      status == 21 ||
      status == 30 ||
      status == 31 ||
      status == 32 ||
      status == 33
    )
      return "warning";
    if (status == 20 || status == 22 || status == 50 || status == 90)
      return "danger";
  };

  const censorString = (str) => {
    const chars = str.length;
    let replace = "";

    const str1 = str.substring(0, 3);
    const str2 = str.substring(chars - 2, chars);

    for (let i = 0; i < chars - (str1.length + str2.length); i++) {
      replace += "*";
    }

    return str1 + replace + str2;
  };

  return {
    toRupiah,
    toRupiahTable,
    toDateTimeTable,
    toDateTable,
    formatDate,
    formatTag,
    censorString,
  };
};

export default useConvert;
