const getUrl = () => {
  const urlApi = "http://122.50.5.62:8007/klik_dn/public/api/office/v1";
  const urlRegister = `${urlApi}/auth/register`;
  const urlLogin = `${urlApi}/auth/login`;
  const urlLogout = `${urlApi}/auth/logout`;
  const urlOrders = `${urlApi}/orders`;
  const urlProducts = `${urlApi}/products`;
  const urlResellers = `${urlApi}/resellers`;
  const urlDashboard = `${urlApi}/dashboard`;
  const urlInventories = `${urlApi}/inventories`;
  const urlOpname = `${urlApi}/opname`;
  const urlProfile = `${urlApi}/profile`;
  const urlCategories = `${urlProducts}/categories`;
  const urlUnits = `${urlProducts}/units`;
  const urlBanks = `${urlApi}/banks`;
  const urlPayments = `${urlApi}/payments`;
  const urlServices = `${urlApi}/delivery-services`;
  const urlUsers = `${urlApi}/users`;
  const urlRoles = `${urlApi}/roles`;
  const urlProvinces = `${urlApi}/provinces`;
  const urlCities = `${urlApi}/cities`;
  const urlStatuses = `${urlApi}/statuses`;

  return {
    urlRegister,
    urlLogin,
    urlLogout,
    urlOrders,
    urlProducts,
    urlResellers,
    urlDashboard,
    urlInventories,
    urlOpname,
    urlProfile,
    urlCategories,
    urlUnits,
    urlBanks,
    urlPayments,
    urlServices,
    urlUsers,
    urlRoles,
    urlProvinces,
    urlCities,
    urlStatuses,
  };
};

export default getUrl;
