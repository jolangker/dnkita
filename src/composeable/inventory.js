import { ref, reactive } from "vue-demi";
import axios from "axios";
import getUrl from "./getUrl";
import { ElMessage } from "element-plus";
import useCookies from "./cookies";
import useErrors from "./errors";
import { useRoute, useRouter } from "vue-router";

const useInvetories = () => {
  const { urlInventories } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized } = useErrors();
  const inventories = ref([]);
  const inventory = ref({});
  const loading = ref(false);
  const router = useRouter();

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });

  axios.defaults.headers.common = headers;

  const getInventories = async () => {
    try {
      const res = await axios.get(urlInventories);
      inventories.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
    }
  };

  const getInventory = (id) => {
    axios.defaults.headers.common = headers;
    axios
      .get(`${urlInventories}/${id}`)
      .then((res) => {
        inventory.value = res.data.data;
      })
      .catch((err) => {
        if (err.response.status === 401) {
          unauthorized();
        }
      });
  };

  const storeInventory = (payload) => {
    loading.value = true;

    axios.defaults.headers.common = headers;
    axios
      .post(urlInventories, payload)
      .then((res) => {
        ElMessage({
          type: "success",
          message: "Berhasil menambahkan barang masuk.",
        });
        router.push({ name: "Barang Masuk View" });
      })
      .catch((err) => {
        if (err.response.status === 401) {
          unauthorized();
        } else {
          ElMessage({
            type: "error",
            message: err.response.data.message,
          });
        }
      })
      .then(() => {
        loading.value = false;
      });
  };

  const destroyInventory = (id) => {
    loading.value = true;

    axios.defaults.headers.common = headers;
    axios
      .delete(`${urlInventories}/${id}`)
      .then((res) => {
        ElMessage({
          type: "success",
          message: "Berhasil menghapus data.",
        });
        router.push({ name: "Barang Masuk View" });
      })
      .catch((err) => {
        if (err.response.status === 401) {
          unauthorized();
        } else {
          ElMessage({
            type: "error",
            message: err.response.data.message,
          });
        }
      })
      .then(() => {
        loading.value = false;
      });
  };

  const updateInventory = (id, payload) => {
    loading.value = true;

    axios.defaults.headers.common = headers;
    axios
      .put(`${urlInventories}/${id}`, payload)
      .then((res) => {
        ElMessage({
          type: "success",
          message: "Berhasil memperbarui data.",
        });
        router.push({ name: "Barang Masuk Details", params: { id } });
      })
      .catch((err) => {
        if (err.response.status === 401) {
          unauthorized();
        } else {
          ElMessage({
            type: "error",
            message: err.response.data.message,
          });
        }
      })
      .then(() => {
        loading.value = false;
      });
  };

  return {
    inventories,
    inventory,
    loading,
    getInventories,
    getInventory,
    storeInventory,
    destroyInventory,
    updateInventory,
  };
};

export default useInvetories;
