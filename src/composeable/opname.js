import axios from "axios";
import { ElMessage } from "element-plus";
import { reactive, ref } from "vue-demi";
import { useRouter } from "vue-router";
import useCookies from "./cookies";
import useErorrs from "./errors";
import getUrl from "./getUrl";

const useOpname = () => {
  const { urlOpname } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized } = useErorrs();
  const opnames = ref([]);
  const opname = ref({});
  const router = useRouter();
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });

  const getOpnames = () => {
    axios.defaults.headers.common = headers;

    axios
      .get(urlOpname)
      .then((res) => {
        opnames.value = res.data.data;
      })
      .catch((err) => {
        if (err.response.status === 401) {
          unauthorized();
        }
      });
  };

  const getOpname = (id) => {
    axios.defaults.headers.common = headers;

    axios
      .get(`${urlOpname}/${id}`)
      .then((res) => {
        opname.value = res.data.data;
      })
      .catch((err) => {
        if (err.res.status === 401) {
          unauthorized();
        }
      });
  };

  const storeOpname = (payload) => {
    loading.value = true;

    axios.defaults.headers.common = headers;
    axios
      .post(`${urlOpname}`, payload)
      .then((res) => {
        ElMessage({
          type: "success",
          message: "Stok opname berhasil ditambahkan",
        });
        router.push({ name: "Stok Opname View" });
      })
      .catch((err) => {
        if (err.response.status === 401) {
          unauthorized();
        }
      })
      .then(() => {
        loading.value = false;
      });
  };

  const updateOpname = (id, payload) => {
    loading.value = true;

    axios.defaults.headers.common = headers;
    axios
      .put(`${urlOpname}/${id}`, payload)
      .then((res) => {
        ElMessage({
          type: "success",
          message: res.data.message,
        });
        router.push({ name: "Stok Opname Details", params: { id } });
      })
      .catch((err) => {
        if (err.response.status === 401) {
          unauthorized();
        }
      })
      .then(() => {
        loading.value = false;
      });
  };

  const destroyOpname = (id) => {
    loading.value = true;

    axios.defaults.headers.common = headers;
    axios
      .delete(`${urlOpname}/${id}`)
      .then((res) => {
        ElMessage({
          type: "success",
          message: "Data berhasil dihapus",
        });
        router.push({ name: "Stok Opname View" });
      })
      .catch((err) => {
        if (err.response.status === 401) {
          unauthorized();
        }
      })
      .then(() => {
        loading.value = false;
      });
  };

  return {
    opnames,
    opname,
    loading,
    getOpnames,
    getOpname,
    storeOpname,
    updateOpname,
    destroyOpname,
  };
};

export default useOpname;
