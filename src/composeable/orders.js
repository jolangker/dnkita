import { ref, reactive, computed } from "vue-demi";
import axios from "axios";
import getUrl from "./getUrl";
import useCookies from "./cookies";
import { ElMessage } from "element-plus";
import { useRoute, useRouter } from "vue-router";
import useErorrs from "./errors";
import { useStore } from "vuex";

const useOrders = () => {
  const { urlOrders } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, forbidden } = useErorrs();
  const route = useRoute();
  const router = useRouter();
  const store = useStore();
  const orders = ref([]);
  const order = ref({});

  const session = getDecodedCookie("session");
  const token = session.access_token;
  const idRole = session.id_role;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getOrders = (status) => {
    axios
      .get(urlOrders, {
        params: {
          kode_status: status,
        },
      })
      .then((res) => {
        orders.value = res.data.data;
      })
      .catch((err) => {
        if (err.response?.status === 401) {
          unauthorized();
        }
      });
  };

  const getOrder = (id) => {
    axios
      .get(`${urlOrders}/${id}`)
      .then((res) => {
        order.value = res.data.data;
      })
      .catch((err) => {
        if (err.response.status === 401) {
          unauthorized();
        }
      });
  };

  const updateStatus = (id, payload, message, type) => {
    if (idRole.value == 1 || idRole.value == 5) {
      forbidden();
    } else {
      axios
        .post(`${urlOrders}/${id}/update-status`, payload)
        .then((res) => {
          ElMessage({
            type: type ? type : "success",
            message,
          });
          router.push({
            name: route.matched[1].name,
          });
        })
        .catch((err) => {
          if (err.response.status === 401) {
            unauthorized();
          }
        });
    }
  };

  const uploadRefund = (id, payload) => {
    axios
      .post(`${urlOrders}/${id}/upload-refund`, payload)
      .then((res) => {
        ElMessage({
          type: "success",
          message: "Berhasil mengirim bukti refund",
        });
        router.push({
          name: route.matched[1].name,
        });
      })
      .catch((err) => {
        if (err.response.status === 401) {
          unauthorized();
        }
      });
  };

  return {
    orders,
    order,
    getOrders,
    getOrder,
    updateStatus,
    uploadRefund,
  };
};

export default useOrders;
