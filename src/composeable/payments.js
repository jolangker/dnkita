import getUrl from "./getUrl";
import useCookies from "./cookies";
import useErrors from "./errors";
import { useRouter } from "vue-router";
import { reactive, ref } from "vue";
import axios from "axios";
import { ElMessage } from "element-plus";

const usePayments = () => {
  const { urlPayments } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError, forbidden } = useErrors();
  const router = useRouter();
  const payments = ref([]);
  const payment = ref({});
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getPayments = async () => {
    loading.value = true;
    try {
      const res = await axios.get(urlPayments);
      payments.value = res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const getPayment = async (id) => {
    loading.value = true;
    try {
      const res = await axios.get(`${urlPayments}/${id}`);
      payment.value = res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const storePayment = async (payload) => {
    loading.value = true;

    try {
      const res = await axios.post(urlPayments, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({
        name: "Master Pembayaran",
      });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const updatePayment = async (id, payload) => {
    loading.value = true;

    try {
      const res = await axios.put(`${urlPayments}/${id}`, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({
        name: "Master Pembayaran Details",
        params: { id },
      });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const destroyPayment = async (id) => {
    loading.value = true;

    try {
      const res = await axios.delete(`${urlPayments}/${id}`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Pembayaran" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  return {
    payments,
    payment,
    loading,
    getPayments,
    getPayment,
    storePayment,
    updatePayment,
    destroyPayment,
  };
};

export default usePayments;
