import { ref, reactive } from "vue-demi";
import axios from "axios";
import getUrl from "./getUrl";
import { ElMessage } from "element-plus";
import useCookies from "./cookies";
import useErrors from "./errors";
import router from "../route";
import { useRouter } from "vue-router";

const useProducts = () => {
  const { urlProducts } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError, forbidden } = useErrors();
  const products = ref([]);
  const product = ref({});
  const router = useRouter();
  const newIdProduct = ref(null);
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getProducts = async () => {
    const res = await axios.get(urlProducts);
    products.value = res.data.data;
  };

  const getProduct = async (id) => {
    loading.value = true;
    try {
      const res = await axios.get(`${urlProducts}/${id}`);
      product.value = res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
    } finally {
      loading.value = false;
    }
  };

  const storeProduct = async (payload, images) => {
    loading.value = true;

    try {
      const res = await axios.post(urlProducts, payload);
      await storeProductImages(res.data.data.id, images);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Produk" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const updateProduct = async (id, payload, images, deletedImages) => {
    loading.value = true;
    try {
      const res = await axios.put(`${urlProducts}/${id}`, payload);
      await destroyProductImages(deletedImages);
      await storeProductImages(id, images);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Produk Details", params: { id } });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const destroyProduct = async (id) => {
    loading.value = true;
    try {
      const res = await axios.delete(`${urlProducts}/${id}`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Produk" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const storeProductImages = async (id, images) => {
    if (images.length) {
      for (const image of images) {
        const fd = new FormData();
        fd.append("id_produk", id);
        fd.append("gambar", image);

        const res = await axios.post(`${urlProducts}/images`, fd);
      }
    }
  };

  const destroyProductImages = async (imagesId) => {
    if (imagesId.length) {
      for (const id of imagesId) {
        const res = await axios.delete(`${urlProducts}/images/${id}`);
      }
    }
  };

  return {
    products,
    product,
    newIdProduct,
    loading,
    getProducts,
    getProduct,
    storeProduct,
    storeProductImages,
    updateProduct,
    destroyProduct,
    destroyProductImages,
  };
};

export default useProducts;
