import axios from "axios";
import { reactive, ref } from "vue";
import getUrl from "./getUrl";
import useCookies from "./cookies";
import useErrors from "./errors";
import { ElMessage } from "element-plus";
import { useRouter } from "vue-router";

const useProfiles = () => {
  const { urlProfile } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError } = useErrors();
  const router = useRouter();
  const profile = ref({});
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getProfile = async (id) => {
    try {
      const res = await axios.get(`${urlProfile}/${id}`);
      profile.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
    }
  };

  const updateProfile = async (id, payload) => {
    loading.value = true;

    try {
      const res = await axios.put(`${urlProfile}/${id}`, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
    } finally {
      loading.value = false;
    }
  };

  const updatePassword = async (id, payload) => {
    loading.value = true;

    try {
      const res = await axios.put(
        `${urlProfile}/${id}/change-password`,
        payload
      );
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Profile" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err.response.data.message);
    } finally {
      loading.value = false;
    }
  };

  const updateImage = async (id, image) => {
    loading.value = true;
    try {
      const fd = new FormData();
      fd.append("gambar", image);
      const res = await axios.post(`${urlProfile}/${id}/photo`, fd);
      await router.go();
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
    } finally {
      loading.value = false;
    }
  };

  return {
    profile,
    loading,
    getProfile,
    updateProfile,
    updatePassword,
    updateImage,
  };
};

export default useProfiles;
