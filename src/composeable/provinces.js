import axios from "axios";
import { ElMessage } from "element-plus";
import { ref, reactive } from "vue";
import { useRouter } from "vue-router";
import useCookies from "./cookies";
import useErrors from "./errors";
import getUrl from "./getUrl";

const useProvinces = () => {
  const { urlProvinces } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError, forbidden } = useErrors();
  const router = useRouter();
  const provinces = ref([]);
  const province = ref({});
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getProvinces = async () => {
    loading.value = true;
    try {
      const res = await axios.get(urlProvinces);
      provinces.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const getProvince = async (id) => {
    loading.value = true;
    try {
      const res = await axios.get(`${urlProvinces}/${id}`);
      province.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const storeProvince = async (payload) => {
    loading.value = true;

    try {
      const res = await axios.post(urlProvinces, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Provinsi" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const updateProvince = async (id, payload) => {
    loading.value = true;

    try {
      const res = await axios.put(`${urlProvinces}/${id}`, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Provinsi Details", params: { id } });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const destroyProvince = async (id) => {
    loading.value = true;

    try {
      const res = await axios.delete(`${urlProvinces}/${id}`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Provinsi" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  return {
    provinces,
    province,
    loading,
    getProvinces,
    getProvince,
    storeProvince,
    updateProvince,
    destroyProvince,
  };
};

export default useProvinces;
