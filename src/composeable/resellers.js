import axios from "axios";
import { ElMessage } from "element-plus";
import { reactive, ref } from "vue";
import { useRouter } from "vue-router";
import useCookies from "./cookies";
import useErrors from "./errors";
import getUrl from "./getUrl";

const useResellers = () => {
  const { urlResellers } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError, forbidden } = useErrors();
  const router = useRouter();
  const resellers = ref([]);
  const reseller = ref({});
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getResellers = async (isVerified) => {
    loading.value = true;
    try {
      const res = await axios.get(urlResellers, {
        params: { is_verified: isVerified },
      });
      resellers.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const getReseller = async (id) => {
    loading.value = true;
    try {
      const res = await axios.get(`${urlResellers}/${id}`);
      reseller.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const storeReseller = async (payload) => {
    loading.value = true;

    try {
      const res = await axios.post(urlResellers, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Reseller" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const updateReseller = async (id, payload) => {
    loading.value = true;

    try {
      const res = await axios.put(`${urlResellers}/${id}`, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Reseller Details", params: { id } });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const destroyReseller = async (id) => {
    loading.value = true;

    try {
      const res = await axios.delete(`${urlResellers}/${id}`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Reseller" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const verifyReseller = async (id) => {
    loading.value = true;

    try {
      const res = await axios.post(`${urlResellers}/${id}/verify`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Validasi Reseller" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  return {
    resellers,
    reseller,
    loading,
    getResellers,
    getReseller,
    storeReseller,
    updateReseller,
    destroyReseller,
    verifyReseller,
  };
};

export default useResellers;
