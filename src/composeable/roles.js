import axios from "axios";
import { ElMessage } from "element-plus";
import { ref, reactive } from "vue";
import { useRouter } from "vue-router";
import useCookies from "./cookies";
import useErrors from "./errors";
import getUrl from "./getUrl";

const useRoles = () => {
  const { urlRoles } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError, forbidden } = useErrors();
  const router = useRouter();
  const roles = ref([]);
  const role = ref({});
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getRoles = async () => {
    loading.value = true;
    try {
      const res = await axios.get(urlRoles);
      roles.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const getRole = async (id) => {
    loading.value = true;
    try {
      const res = await axios.get(`${urlRoles}/${id}`);
      role.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const storeRole = async (payload) => {
    loading.value = true;

    try {
      const res = await axios.post(urlRoles, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Role" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const updateRole = async (id, payload) => {
    loading.value = true;

    try {
      const res = await axios.put(`${urlRoles}/${id}`, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Role Details", params: { id } });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const destroyRole = async (id) => {
    loading.value = true;

    try {
      const res = await axios.delete(`${urlRoles}/${id}`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Role" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  return {
    roles,
    role,
    loading,
    getRoles,
    getRole,
    storeRole,
    updateRole,
    destroyRole,
  };
};

export default useRoles;
