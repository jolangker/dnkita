import getUrl from "./getUrl";
import useCookies from "./cookies";
import useErrors from "./errors";
import { useRouter } from "vue-router";
import { reactive, ref } from "vue";
import axios from "axios";
import { ElMessage } from "element-plus";

const useServices = () => {
  const { urlServices } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError, forbidden } = useErrors();
  const router = useRouter();
  const services = ref([]);
  const service = ref({});
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getServices = async () => {
    loading.value = true;
    try {
      const res = await axios.get(urlServices);
      services.value = res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const getService = async (id) => {
    loading.value = true;
    try {
      const res = await axios.get(`${urlServices}/${id}`);
      service.value = res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const storeService = async (payload) => {
    loading.value = true;

    try {
      const res = await axios.post(urlServices, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Jasa Pengiriman" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const updateService = async (id, payload) => {
    loading.value = true;

    try {
      const res = await axios.put(`${urlServices}/${id}`, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({
        name: "Master Jasa Pengiriman Details",
        params: { id },
      });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const destroyService = async (id) => {
    loading.value = true;

    try {
      const res = await axios.delete(`${urlServices}/${id}`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Jasa Pengiriman" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  return {
    services,
    service,
    loading,
    getServices,
    getService,
    storeService,
    updateService,
    destroyService,
  };
};

export default useServices;
