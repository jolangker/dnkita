import axios from "axios";
import { ElMessage } from "element-plus";
import { ref, reactive } from "vue";
import { useRouter } from "vue-router";
import useCookies from "./cookies";
import useErrors from "./errors";
import getUrl from "./getUrl";

const useStatuses = () => {
  const { urlStatuses } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError, entityError, forbidden } = useErrors();
  const router = useRouter();
  const statuses = ref([]);
  const status = ref({});
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getStatuses = async () => {
    loading.value = true;
    try {
      const res = await axios.get(urlStatuses);
      statuses.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const getStatus = async (id) => {
    loading.value = true;
    try {
      const res = await axios.get(`${urlStatuses}/${id}`);
      status.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const storeStatus = async (payload) => {
    loading.value = true;

    try {
      const res = await axios.post(urlStatuses, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Status" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      if (err.response?.status == 422)
        return entityError(err.response.data.message);
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const updateStatus = async (id, payload) => {
    loading.value = true;

    try {
      const res = await axios.put(`${urlStatuses}/${id}`, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Status Details", params: { id } });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      if (err.response?.status == 422)
        return entityError(err.response.data.message);
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const destroyStatus = async (id) => {
    loading.value = true;

    try {
      const res = await axios.delete(`${urlStatuses}/${id}`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Status" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  return {
    statuses,
    status,
    loading,
    getStatuses,
    getStatus,
    storeStatus,
    updateStatus,
    destroyStatus,
  };
};

export default useStatuses;
