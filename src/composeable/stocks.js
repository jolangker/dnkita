import { ref } from "vue-demi";
import axios from "axios";
import getUrl from "./getUrl";

const useStocks = () => {
  const { urlStocks } = getUrl();
  const stocks = ref([]);

  const getStocks = () => {
    axios.get(urlStocks).then((res) => {
      stocks.value = res.data;
    });
  };

  return {
    stocks,
    getStocks,
  };
};

export default useStocks;
