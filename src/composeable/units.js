import { ref, reactive } from "vue-demi";
import axios from "axios";
import getUrl from "./getUrl";
import useCookies from "./cookies";
import useErorrs from "./errors";
import { ElMessage } from "element-plus";
import { useRouter } from "vue-router";

const useUnits = () => {
  const { urlUnits } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError, forbidden } = useErorrs();
  const router = useRouter();
  const units = ref([]);
  const unit = ref({});
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;
  const idRole = session.id_role;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getUnits = async () => {
    loading.value = true;
    try {
      const res = await axios.get(urlUnits);
      units.value = res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err.response.data.message);
    } finally {
      loading.value = false;
    }
  };

  const getUnit = async (id) => {
    loading.value = true;
    try {
      const res = await axios.get(`${urlUnits}/${id}`);
      unit.value = res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err.response.data.message);
    } finally {
      loading.value = false;
    }
  };

  const storeUnit = async (payload) => {
    loading.value = true;

    try {
      const res = await axios.post(urlUnits, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Satuan Produk" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const updateUnit = async (id, payload) => {
    loading.value = true;

    try {
      const res = await axios.put(`${urlUnits}/${id}`, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({
        name: "Master Satuan Produk Details",
        params: { id },
      });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const destroyUnit = async (id, message) => {
    loading.value = true;
    try {
      const res = await axios.delete(`${urlUnits}/${id}`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master Satuan Produk" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  return {
    units,
    unit,
    loading,
    getUnits,
    getUnit,
    storeUnit,
    updateUnit,
    destroyUnit,
  };
};

export default useUnits;
