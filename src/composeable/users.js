import axios from "axios";
import { ElMessage } from "element-plus";
import { reactive, ref } from "vue";
import { useRouter } from "vue-router";
import useCookies from "./cookies";
import useErrors from "./errors";
import getUrl from "./getUrl";

const useUsers = () => {
  const { urlUsers } = getUrl();
  const { getDecodedCookie } = useCookies();
  const { unauthorized, othersError, forbidden } = useErrors();
  const router = useRouter();
  const users = ref([]);
  const user = ref({});
  const loading = ref(false);

  const session = getDecodedCookie("session");
  const token = session.access_token;

  const headers = reactive({
    Authorization: `Bearer ${token}`,
    "x-user-status": "office",
  });
  axios.defaults.headers.common = headers;

  const getUsers = async () => {
    loading.value = true;
    try {
      const res = await axios.get(urlUsers);
      users.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const getUser = async (id) => {
    loading.value = true;
    try {
      const res = await axios.get(`${urlUsers}/${id}`);
      user.value = await res.data.data;
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const storeUser = async (payload) => {
    loading.value = true;

    try {
      const res = await axios.post(urlUsers, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master User" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const updateUser = async (id, payload) => {
    loading.value = true;

    try {
      const res = await axios.put(`${urlUsers}/${id}`, payload);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master User Details", params: { id } });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  const destroyUser = async (id) => {
    loading.value = true;

    try {
      const res = await axios.delete(`${urlUsers}/${id}`);
      await ElMessage({
        type: "success",
        message: res.data.message,
      });
      await router.push({ name: "Master User" });
    } catch (err) {
      if (err.response?.status == 401) return unauthorized();
      othersError(err);
    } finally {
      loading.value = false;
    }
  };

  return {
    users,
    user,
    loading,
    getUsers,
    getUser,
    storeUser,
    updateUser,
    destroyUser,
  };
};

export default useUsers;
