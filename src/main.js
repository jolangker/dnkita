import { createApp } from "vue";
import App from "./App.vue";
import ElementPlus from "element-plus";
import id from "element-plus/es/locale/lang/id";
import router from "./route";
import store from "./store";
import VueApexCharts from "vue3-apexcharts";
import "@/assets/styles/element.scss";
import "@/assets/styles/tailwind.css";
import "image-preview-vue/lib/imagepreviewvue.css";

const app = createApp(App);

app.use(ElementPlus, {
  locale: id,
});
app.use(router);
app.use(store);
app.use(VueApexCharts);

app.mount("#app");
