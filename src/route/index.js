import { ElMessage } from "element-plus";
import { createRouter, createWebHistory } from "vue-router";
import useCookies from "@/composeable/cookies";
import { computed, ref, watch } from "vue";
import {
  Box,
  Connection,
  Document,
  CircleCheck,
  DataLine,
  Files,
  RefreshLeft,
  Money,
  User,
  Fries,
  SoldOut,
  Collection,
  Grid,
  Cherry,
  CreditCard,
  Wallet,
  Ship,
  Finished,
  Switch as SwitchIcon,
  OfficeBuilding,
  School,
  MagicStick,
} from "@element-plus/icons";

const { getDecodedCookie } = useCookies();

const idRole = ref(null);

const routes = [
  {
    path: "/",
    name: "Home",
    component: () => import("@/views/Home.vue"),
    redirect: "/dashboard",
    meta: {
      requireAuth: true,
    },
    children: [
      {
        path: "dashboard",
        name: "Dashboard",
        component: () => import("@/views/Dashboard.vue"),
        meta: {
          group: "General",
          icon: DataLine,
          restrictAccess: [],
          restrictAction: [],
        },
      },
      {
        path: "master-produk",
        name: "Master Produk",
        component: () => import("@/views/master_product/Index.vue"),
        redirect: "/master-produk/view",
        meta: {
          group: "Master",
          icon: Files,
          restrictAccess: [],
          restrictAction: [2, 5],
        },
        children: [
          {
            path: "view",
            name: "Master Produk View",
            component: () => import("@/views/master_product/View.vue"),
          },
          {
            path: "add",
            name: "Master Produk Add",
            component: () => import("@/views/master_product/Add.vue"),
          },
          {
            path: "details/:id",
            name: "Master Produk Details",
            component: () => import("@/views/master_product/Details.vue"),
          },
          {
            path: "edit/:id",
            name: "Master Produk Edit",
            component: () => import("@/views/master_product/Edit.vue"),
          },
        ],
      },
      {
        path: "master-kategori",
        name: "Master Kategori",
        component: () => import("@/views/master_category/Index.vue"),
        redirect: "/master-kategori/view",
        meta: {
          group: "Master",
          icon: Grid,
          restrictAccess: [2],
          restrictAction: [2, 5],
        },
        children: [
          {
            path: "view",
            name: "Master Kategori View",
            component: () => import("@/views/master_category/View.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
          {
            path: "add",
            name: "Master Kategori Add",
            component: () => import("@/views/master_category/Add.vue"),
            meta: {
              restrictAccess: [2, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Master Kategori Edit",
            component: () => import("@/views/master_category/Edit.vue"),
            meta: {
              restrictAccess: [2, 5],
            },
          },
          {
            path: "details/:id",
            name: "Master Kategori Details",
            component: () => import("@/views/master_category/Details.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
        ],
      },
      {
        path: "master-satuan",
        name: "Master Satuan Produk",
        component: () => import("@/views/master_unit/Index.vue"),
        redirect: "/master-satuan/view",
        meta: {
          group: "Master",
          icon: Cherry,
          restrictAccess: [2],
          restrictAction: [2, 5],
        },
        children: [
          {
            path: "view",
            name: "Master Satuan Produk View",
            component: () => import("@/views/master_unit/View.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
          {
            path: "add",
            name: "Master Satuan Produk Add",
            component: () => import("@/views/master_unit/Add.vue"),
            meta: {
              restrictAccess: [2, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Master Satuan Produk Edit",
            component: () => import("@/views/master_unit/Edit.vue"),
            meta: {
              restrictAccess: [2, 5],
            },
          },
          {
            path: "details/:id",
            name: "Master Satuan Produk Details",
            component: () => import("@/views/master_unit/Details.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
        ],
      },
      {
        path: "master-bank",
        name: "Master Bank",
        component: () => import("@/views/master_bank/Index.vue"),
        redirect: "/master-bank/view",
        meta: {
          group: "Master",
          icon: CreditCard,
          restrictAccess: [3],
          restrictAction: [2, 3, 5],
        },
        children: [
          {
            path: "view",
            name: "Master Bank View",
            component: () => import("@/views/master_bank/View.vue"),
            meta: {
              restrictAccess: [3],
            },
          },
          {
            path: "add",
            name: "Master Bank Add",
            component: () => import("@/views/master_bank/Add.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Master Bank Edit",
            component: () => import("@/views/master_bank/Edit.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "details/:id",
            name: "Master Bank Details",
            component: () => import("@/views/master_bank/Details.vue"),
            meta: {
              restrictAccess: [3],
            },
          },
        ],
      },
      {
        path: "master-metode-pembayaran",
        name: "Master Pembayaran",
        component: () => import("@/views/master_payment/Index.vue"),
        redirect: "/master-metode-pembayaran/view",
        meta: {
          group: "Master",
          icon: Wallet,
          restrictAccess: [3],
          restrictAction: [2, 3, 5],
        },
        children: [
          {
            path: "view",
            name: "Master Pembayaran View",
            component: () => import("@/views/master_payment/View.vue"),
            meta: {
              restrictAccess: [3],
            },
          },
          {
            path: "add",
            name: "Master Pembayaran Add",
            component: () => import("@/views/master_payment/Add.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Master Pembayaran Edit",
            component: () => import("@/views/master_payment/Edit.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "details/:id",
            name: "Master Pembayaran Details",
            component: () => import("@/views/master_payment/Details.vue"),
            meta: {
              restrictAccess: [3],
            },
          },
        ],
      },
      {
        path: "master-jasa-pengiriman",
        name: "Master Jasa Pengiriman",
        component: () => import("@/views/master_service/Index.vue"),
        redirect: "/master-jasa-pengiriman/view",
        meta: {
          group: "Master",
          icon: Ship,
          restrictAccess: [2],
          restrictAction: [2, 3, 5],
        },
        children: [
          {
            path: "view",
            name: "Master Jasa Pengiriman View",
            component: () => import("@/views/master_service/View.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
          {
            path: "add",
            name: "Master Jasa Pengiriman Add",
            component: () => import("@/views/master_service/Add.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Master Jasa Pengiriman Edit",
            component: () => import("@/views/master_service/Edit.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "details/:id",
            name: "Master Jasa Pengiriman Details",
            component: () => import("@/views/master_service/Details.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
        ],
      },
      {
        path: "master-reseller",
        name: "Master Reseller",
        component: () => import("@/views/master_reseller/Index.vue"),
        redirect: "/master-reseller/view",
        meta: {
          group: "Master",
          icon: Connection,
          restrictAccess: [2, 3],
          restrictAction: [2, 3, 5],
        },
        children: [
          {
            path: "view",
            name: "Master Reseller View",
            component: () => import("@/views/master_reseller/View.vue"),
            meta: {
              restrictAccess: [2, 3],
            },
          },
          {
            path: "add",
            name: "Master Reseller Add",
            component: () => import("@/views/master_reseller/Add.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Master Reseller Edit",
            component: () => import("@/views/master_reseller/Edit.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "details/:id",
            name: "Master Reseller Details",
            component: () => import("@/views/master_reseller/Details.vue"),
            meta: {
              restrictAccess: [2, 3],
            },
          },
        ],
      },
      {
        path: "master-user",
        name: "Master User",
        component: () => import("@/views/master_user/Index.vue"),
        redirect: "/master-user/view",
        meta: {
          group: "Master",
          icon: User,
          restrictAccess: [2, 3],
          restrictAction: [2, 3, 5],
        },
        children: [
          {
            path: "view",
            name: "Master User View",
            component: () => import("@/views/master_user/View.vue"),
            meta: {
              restrictAccess: [2, 3],
            },
          },
          {
            path: "add",
            name: "Master User Add",
            component: () => import("@/views/master_user/Add.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Master User Edit",
            component: () => import("@/views/master_user/Edit.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "details/:id",
            name: "Master User Details",
            component: () => import("@/views/master_user/Details.vue"),
            meta: {
              restrictAccess: [2, 3],
            },
          },
        ],
      },
      {
        path: "master-role",
        name: "Master Role",
        component: () => import("@/views/master_role/Index.vue"),
        redirect: "/master-role/view",
        meta: {
          group: "Master",
          icon: SwitchIcon,
          restrictAccess: [2, 3],
          restrictAction: [2, 3, 5],
        },
        children: [
          {
            path: "view",
            name: "Master Role View",
            component: () => import("@/views/master_role/View.vue"),
            meta: {
              restrictAccess: [2, 3],
            },
          },
          {
            path: "add",
            name: "Master Role Add",
            component: () => import("@/views/master_role/Add.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Master Role Edit",
            component: () => import("@/views/master_role/Edit.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "details/:id",
            name: "Master Role Details",
            component: () => import("@/views/master_role/Details.vue"),
            meta: {
              restrictAccess: [2, 3],
            },
          },
        ],
      },
      {
        path: "master-provinsi",
        name: "Master Provinsi",
        component: () => import("@/views/master_province/Index.vue"),
        redirect: "/master-provinsi/view",
        meta: {
          group: "Master",
          icon: OfficeBuilding,
          restrictAccess: [2, 3],
          restrictAction: [2, 3, 5],
        },
        children: [
          {
            path: "view",
            name: "Master Provinsi View",
            component: () => import("@/views/master_province/View.vue"),
            meta: {
              restrictAccess: [2, 3],
            },
          },
          {
            path: "add",
            name: "Master Provinsi Add",
            component: () => import("@/views/master_province/Add.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Master Provinsi Edit",
            component: () => import("@/views/master_province/Edit.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "details/:id",
            name: "Master Provinsi Details",
            component: () => import("@/views/master_province/Details.vue"),
            meta: {
              restrictAccess: [2, 3],
            },
          },
        ],
      },
      {
        path: "master-kabupaten-kota",
        name: "Master Kabupaten Kota",
        component: () => import("@/views/master_city/Index.vue"),
        redirect: "/master-kabupaten-kota/view",
        meta: {
          group: "Master",
          icon: School,
          restrictAccess: [2, 3],
          restrictAction: [2, 3, 5],
        },
        children: [
          {
            path: "view",
            name: "Master Kabupaten Kota View",
            component: () => import("@/views/master_city/View.vue"),
            meta: {
              restrictAccess: [2, 3],
            },
          },
          {
            path: "add",
            name: "Master Kabupaten Kota Add",
            component: () => import("@/views/master_city/Add.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Master Kabupaten Kota Edit",
            component: () => import("@/views/master_city/Edit.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "details/:id",
            name: "Master Kabupaten Kota Details",
            component: () => import("@/views/master_city/Details.vue"),
            meta: {
              restrictAccess: [2, 3],
            },
          },
        ],
      },
      {
        path: "master-status",
        name: "Master Status",
        component: () => import("@/views/master_status/Index.vue"),
        redirect: "/master-status/view",
        meta: {
          group: "Master",
          icon: MagicStick,
          restrictAccess: [2, 3],
          restrictAction: [2, 3, 5],
        },
        children: [
          {
            path: "view",
            name: "Master Status View",
            component: () => import("@/views/master_status/View.vue"),
            meta: {
              restrictAccess: [2, 3],
            },
          },
          {
            path: "add",
            name: "Master Status Add",
            component: () => import("@/views/master_status/Add.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Master Status Edit",
            component: () => import("@/views/master_status/Edit.vue"),
            meta: {
              restrictAccess: [2, 3, 5],
            },
          },
          {
            path: "details/:id",
            name: "Master Status Details",
            component: () => import("@/views/master_status/Details.vue"),
            meta: {
              restrictAccess: [2, 3],
            },
          },
        ],
      },
      {
        path: "stok",
        name: "Stock",
        component: () => import("@/views/stock/Index.vue"),
        redirect: "/stok/view",
        meta: {
          group: "Master",
          icon: Fries,
          restrictAccess: [],
          restrictAction: [],
        },
        children: [
          {
            path: "view",
            name: "Stock View",
            component: () => import("@/views/stock/View.vue"),
            meta: {
              restrictAccess: [],
            },
          },
        ],
      },
      {
        path: "barang-masuk",
        name: "Barang Masuk",
        component: () => import("@/views/inventory_in/Index.vue"),
        redirect: "/barang-masuk/view",
        meta: {
          group: "Transaction",
          icon: SoldOut,
          restrictAccess: [2],
          restrictAction: [1, 5],
        },
        children: [
          {
            path: "view",
            name: "Barang Masuk View",
            component: () => import("@/views/inventory_in/View.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
          {
            path: "details/:id",
            name: "Barang Masuk Details",
            component: () => import("@/views/inventory_in/Details.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
          {
            path: "add",
            name: "Barang Masuk Add",
            component: () => import("@/views/inventory_in/Add.vue"),
            meta: {
              restrictAccess: [1, 2, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Barang Masuk Edit",
            component: () => import("@/views/inventory_in/Edit.vue"),
            meta: {
              restrictAccess: [1, 2, 5],
            },
          },
        ],
      },
      {
        path: "stok-opname",
        name: "Stok Opname",
        component: () => import("@/views/opname/Index.vue"),
        redirect: "/stok-opname/view",
        meta: {
          group: "Transaction",
          icon: Collection,
          restrictAccess: [2],
          restrictAction: [1, 5],
        },
        children: [
          {
            path: "view",
            name: "Stok Opname View",
            component: () => import("@/views/opname/View.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
          {
            path: "details/:id",
            name: "Stok Opname Details",
            component: () => import("@/views/opname/Detail.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
          {
            path: "add",
            name: "Stok Opname Add",
            component: () => import("@/views/opname/Add.vue"),
            meta: {
              restrictAccess: [1, 2, 5],
            },
          },
          {
            path: "edit/:id",
            name: "Stok Opname Edit",
            component: () => import("@/views/opname/Edit.vue"),
            meta: {
              restrictAccess: [1, 2, 5],
            },
          },
        ],
      },
      {
        path: "validasi-reseller",
        name: "Validasi Reseller",
        component: () => import("@/views/validation/Index.vue"),
        redirect: "/validasi-reseller/view",
        meta: {
          group: "Transaction",
          icon: Finished,
          restrictAccess: [3],
          restrictAction: [1, 3, 5],
        },
        children: [
          {
            path: "view",
            name: "Validasi Reseller View",
            component: () => import("@/views/validation/View.vue"),
            meta: {
              restrictAccess: [3],
            },
          },
          {
            path: "details/:id",
            name: "Validasi Reseller Details",
            component: () => import("@/views/validation/Details.vue"),
            meta: {
              restrictAccess: [3],
            },
          },
        ],
      },
      {
        path: "daftar-pesanan",
        name: "Daftar Pesanan",
        component: () => import("@/views/orders_list/Index.vue"),
        redirect: "/daftar-pesanan/view",
        meta: {
          group: "Transaction",
          icon: Document,
          restrictAccess: [],
          restrictAction: [],
        },
        children: [
          {
            path: "view",
            nama: "Daftar Pesanan View",
            component: () => import("@/views/orders_list/OrdersList.vue"),
            meta: {
              restrictAccess: [],
            },
          },
          {
            path: "details/:id",
            name: "Daftar Pesanan Details",
            component: () => import("@/views/orders_list/Details.vue"),
            meta: {
              restrictAccess: [],
            },
          },
        ],
      },
      {
        path: "verifikasi-pembayaran",
        name: "Verifikasi Pembayaran",
        component: () => import("@/views/orders_payment/Index.vue"),
        redirect: "/verifikasi-pembayaran/view",
        meta: {
          group: "Transaction",
          icon: Money,
          restrictAccess: [3],
          restrictAction: [1, 3, 5],
        },
        children: [
          {
            path: "view",
            name: "Verifikasi Pembayaran",
            component: () =>
              import("@/views/orders_payment/VerificationPayment.vue"),
            meta: {
              restrictAccess: [3],
            },
          },
          {
            path: "details/:id",
            name: "Verifikasi Pembayaran Details",
            component: () => import("@/views/orders_payment/Details.vue"),
            meta: {
              restrictAccess: [3],
            },
          },
        ],
      },
      {
        path: "konfirmasi-stok",
        name: "Konfirmasi Stok",
        component: () => import("@/views/stock_confirm/Index.vue"),
        redirect: "/konfirmasi-stok/view",
        meta: {
          group: "Transaction",
          icon: CircleCheck,
          restrictAccess: [2],
          restrictAction: [1, 2, 5],
        },
        children: [
          {
            path: "view",
            name: "Konfirmasi Stok View",
            component: () => import("@/views/stock_confirm/View.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
          {
            path: "details/:id",
            name: "Konfirmasi Stok Details",
            component: () => import("@/views/stock_confirm/Details.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
        ],
      },
      {
        path: "pengiriman",
        name: "Pengiriman",
        component: () => import("@/views/delivery/Index.vue"),
        redirect: "/pengiriman/view",
        meta: {
          group: "Transaction",
          icon: Box,
          restrictAccess: [2],
          restrictAction: [1, 2, 5],
        },
        children: [
          {
            path: "view",
            name: "Pengiriman View",
            component: () => import("@/views/delivery/View.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
          {
            path: "details/:id",
            name: "Pengiriman Details",
            component: () => import("@/views/delivery/Details.vue"),
            meta: {
              restrictAccess: [2],
            },
          },
        ],
      },
      {
        path: "pengembalian",
        name: "Pengembalian",
        component: () => import("@/views/return/Index.vue"),
        redirect: "/pengembalian/view",
        meta: {
          group: "Transaction",
          icon: RefreshLeft,
          restrictAccess: [3],
          restrictAction: [1, 3, 5],
        },
        children: [
          {
            path: "view",
            name: "Pengembalian View",
            component: () => import("@/views/return/View.vue"),
            meta: {
              restrictAccess: [3],
            },
          },
          {
            path: "details/:id",
            name: "Pengembalian Details",
            component: () => import("@/views/return/Details.vue"),
            meta: {
              restrictAccess: [3],
            },
          },
        ],
      },
      {
        path: "profil",
        name: "Profile",
        component: () => import("@/views/profile/Index.vue"),
        redirect: "/profil/view",
        meta: {
          group: "None",
          restrictAccess: [],
          restrictAction: [],
        },
        children: [
          {
            path: "view",
            name: "Profile View",
            component: () => import("@/views/profile/View.vue"),
            meta: {
              restrictAccess: [],
            },
          },
          {
            path: "ganti-password",
            name: "Profile Change Password",
            component: () => import("@/views/profile/ChangePassword.vue"),
            meta: {
              restrictAccess: [],
            },
          },
        ],
      },
      {
        path: "/not-allowed",
        name: "Not Allowed",
        component: () => import("@/views/NotAllowed.vue"),
        meta: {
          group: "None",
          restrictAccess: [],
          restrictAction: [],
        },
      },
    ],
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("@/views/Login.vue"),
  },
  {
    path: "/print",
    name: "Print",
    component: () => import("@/views/Print.vue"),
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.name === "Login") {
    const session = getDecodedCookie("session");
    if (session) {
      next({ name: "Dashboard" });
    } else {
      next();
    }
  } else {
    const session = getDecodedCookie("session");
    if (!session) {
      next({ name: "Login" });
    } else {
      if (to.meta.restrictAccess?.includes(session.id_role)) {
        next({ name: "Not Allowed" });
      } else {
        next();
      }
    }
  }
});

export default router;
