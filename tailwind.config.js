module.exports = {
  mode: "jit",
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        nunito: "'Nunito Sans', sans-serif;",
      },
      colors: {
        primary: {
          DEFAULT: "#7367F0",
          50: "#FFFFFF",
          100: "#FCFCFF",
          200: "#DAD6FB",
          300: "#B7B1F7",
          400: "#958CF4",
          500: "#7367F0",
          600: "#4434EB",
          700: "#2515D2",
          800: "#1C109F",
          900: "#130B6C",
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
